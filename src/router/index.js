import Vue from 'vue'
import Router from 'vue-router'
import SimpleBomber from '@/components/SimpleBomber'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SimpleBomber',
      component: SimpleBomber
    }
  ]
})
